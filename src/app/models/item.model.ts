export class Item {
    id: number;
    categoryId: number;
    code: string;
    name: string;
    uom: string;
    cost: number;
    price: number;
    isActive: boolean;
}

export class Category {
    id: number;
    name: string;
    itemCount: number;
    isActive: boolean;
}