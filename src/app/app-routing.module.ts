import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'sales', canActivate: [AuthGuard], loadChildren: './pages/sales/sales.module#SalesPageModule' },
  { path: 'settle', canActivate: [AuthGuard], loadChildren: './pages/settle/settle.module#SettlePageModule' },
  { path: 'settings', canActivate: [AuthGuard], loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'items', canActivate: [AuthGuard], loadChildren: './pages/items/items.module#ItemsPageModule' },
  { path: 'sales-report', canActivate: [AuthGuard], loadChildren: './pages/sales-report/sales-report.module#SalesReportPageModule' },
  { path: 'pricing', canActivate: [AuthGuard], loadChildren: './pages/pricing/pricing.module#PricingPageModule' },
  { path: 'category', canActivate: [AuthGuard], loadChildren: './pages/items/category/category.module#CategoryPageModule' },
  { path: 'item', loadChildren: './pages/items/item/item.module#ItemPageModule' },
  { path: 'discount', canActivate: [AuthGuard], loadChildren: './pages/settle/discount/discount.module#DiscountPageModule' },  
  { path: 'per-invoice', canActivate: [AuthGuard], loadChildren: './pages/sales-report/per-invoice/per-invoice.module#PerInvoicePageModule' },
  { path: 'database', canActivate: [AuthGuard], loadChildren: './pages/database/database.module#DatabasePageModule' },
  { path: 'config', canActivate: [AuthGuard], loadChildren: './pages/config/config.module#ConfigPageModule' },
  { path: 'restored', loadChildren: './pages/database/restored/restored.module#RestoredPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
