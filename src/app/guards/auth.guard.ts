import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SettingsService } from '../services/settings.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private settings: SettingsService, private router: Router) {}

  canActivate(): boolean {
    if (this.settings.clientCode) return true;
    else {
      this.router.navigateByUrl('/home');

      return false;
    }
  }
}
