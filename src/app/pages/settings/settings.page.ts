import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  version = '1.0.0';

  constructor(private router: Router, private appVersion: AppVersion) { }

  async ngOnInit() {
    this.version = await this.appVersion.getVersionNumber();
  }

  items() {
    this.router.navigateByUrl('items');
  }

  salesReport() {
    this.router.navigateByUrl('sales-report');
  }

  settings() {
    this.router.navigateByUrl('config');
  }

  pricing() {
    this.router.navigateByUrl('pricing');
  }

  database() {
    this.router.navigateByUrl('database');
  }
}
