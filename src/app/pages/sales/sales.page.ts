import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController, Platform } from '@ionic/angular';
import { SalesService } from 'src/app/services/sales.service';
import { Item } from 'src/app/models/item.model';
import { ItemService } from 'src/app/services/item.service';
import { Subscription } from 'rxjs';
import { DbService } from 'src/app/services/db/db.service';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.page.html',
  styleUrls: ['./sales.page.scss'],
})
export class SalesPage implements OnInit {
  hasLoaded: boolean;
  payLater: boolean;
  expired: boolean;

  categories = [];
  selectedCategory: number;

  items = [];
  searchItems = [];
  searchKey = '';

  cart = {
    quantity: 0,
    total: 0
  }
  
  $cart: Subscription;  
  
  pendingOrderCount = 0;

  constructor(private router: Router, 
              private db: DbService,
              private alertCtrl: AlertController, 
              private toastCtrl: ToastController, 
              private itemService: ItemService,
              private settingsService: SettingsService,
              private service: SalesService) { }

  ngOnInit() {
    this.cartSummary();
    
    this.db.getDatabaseState().subscribe(ready => {      
      if (ready) this.getCategories();
    })
    
    this.itemService.$category.subscribe(() => this.getCategories());

    this.$cart = this.service.$clearCart.subscribe(value => {
      if (value) {
        this.cart.quantity = 0;
        this.cart.total = 0;
      }
    });

    this.service.$cart.subscribe(() => {
      this.cartSummary();
    });

    this.service.$pendingOrder.subscribe(count => this.pendingOrderCount = count);
    this.service.pendingOrdersCount();
  }

  ngOnDestroy() {
    if (this.$cart) this.$cart.unsubscribe();
  }

  ionViewWillEnter() {    
    this.payLater = this.settingsService.payLater;
    this.expired = this.settingsService.expired;
  }

  ionViewDidEnter() {
    this.items.map(item => item["cartQuantity"] = this.getCartQuantity(item.id));
    this.searchItems.map(item => item["cartQuantity"] = this.getCartQuantity(item.id));
  }

  async getCategories() {
    
    try {
      this.categories = await this.itemService.getCategories();

      this.hasLoaded = true;
    } catch (error) {
      
    }
  }

  async getItems(categoryId: number) {
    this.searchItems = [];
    this.searchKey = '';

    this.selectedCategory = categoryId;
    const items = await this.itemService.getItems(categoryId);
    items.map(item => item["cartQuantity"] = this.getCartQuantity(item.id));
    
    this.items = items;
  }

  async search(event) {
    if (event) {
      const searchkey = event.target.value;
      const searchItems = searchkey ? await this.itemService.searchItems(searchkey) : [];  
      searchItems.map(item => item["cartQuantity"] = this.getCartQuantity(item.id));
      this.searchItems = searchItems;
    }
  }

  settle() {
    if (this.settingsService.payLater) this.router.navigateByUrl('sales/pending');
    else if (this.cart.quantity) this.router.navigateByUrl('settle');
  }

  pendingOrders() {
    this.router.navigateByUrl('sales/pending?viewMode=true');
  }

  settings() {
    this.router.navigateByUrl('settings');
  }

  gotoCategory() {
    this.router.navigateByUrl('items');
  }

  renew() {
    this.router.navigateByUrl('pricing');
  }

  async buy(item: Item) {
    const alert = await this.alertCtrl.create({
      header: item.name,
      message: "Price: " + item.price,
      mode: "ios",
      cssClass: 'buy',
      backdropDismiss: false,
      inputs: [
        {
          name: 'quantity',
          type: 'number',
          placeholder: "1"          
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Buy',
          handler: async (data) => {

            data.quantity = data.quantity || 1;

            if (data.quantity && data.quantity > 0) {
              this.addToCart(item, parseFloat(data.quantity));
            }

          }
        }        
      ]
    });

    alert.present();
  }

  async addToCart(item: Item, quantity: number) {

    let cartItems = this.service.getCartItems();
    let cartItem = cartItems.find(x => x.id == item.id);

    if (cartItem) {

      cartItem.code = item.code;
      cartItem.name = item.name;
      cartItem.uom = item.uom;
      cartItem.quantity = quantity;
      cartItem.price = item.price;
      cartItem.amount = quantity * item.price;

      this.service.setCartItems(cartItems);

    } else {
      this.service.addToCart({
        uid: new Date().getTime(),
        id: item.id,
        code: item.code,
        name: item.name,
        uom: item.uom,
        quantity: quantity,
        price: item.price,
        amount: quantity * item.price
      });
    }

    item["cartQuantity"] = quantity;

    this.cartSummary();
    this.showToast(item.name, quantity);
  }
  
  cartSummary() {
    const cartItems = this.service.getCartItems();

    this.cart.quantity = cartItems.length;
    this.cart.total = cartItems.reduce( (tot, record) => {
        return tot + record.amount;
    }, 0);
  }

  getCartQuantity(itemId): any {
    const item = this.service.getCartItems().find(x => x.id == itemId);
    if (item) return item.quantity;

    return "";
  }

  async showToast(name: string, quantity: number) {
    const toast = await this.toastCtrl.create({
      message: quantity + ' - ' + name,
      duration: 1000,
      position: 'top',
      mode: 'ios',
      color: 'success'
    });
    
    toast.present();
  }
}
