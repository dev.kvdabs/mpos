import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SalesPage } from './sales.page';
import { PendingComponent } from './pending/pending.component';
import { PendingOrderComponent } from './pending/pending-order/pending-order.component';

const routes: Routes = [
  {
    path: '',
    component: SalesPage
  },
  {
    path: 'pending',
    component: PendingComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [PendingOrderComponent],
  declarations: [SalesPage, PendingComponent, PendingOrderComponent]
})
export class SalesPageModule {}
