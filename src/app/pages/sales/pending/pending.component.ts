import { Component, OnInit } from '@angular/core';
import { AlertController, ActionSheetController, ModalController } from '@ionic/angular';
import { SalesService } from 'src/app/services/sales.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Item } from 'src/app/models/item.model';
import { PendingOrderComponent } from './pending-order/pending-order.component';

@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss'],
})
export class PendingComponent implements OnInit {

  orders = [];
  hasCartItems = false;
  selected: any = {};
  items = [];

  viewMode: boolean;

  constructor(private salesService: SalesService, private modalCtrl: ModalController, private actionSheetCtrl: ActionSheetController, private alertCtrl: AlertController, private route: ActivatedRoute) { }

  ngOnInit() {    
    this.viewMode = this.route.snapshot.queryParams.viewMode !== undefined;
    this.getPendingOrders();
  }

  getPendingOrders() {
    this.orders = this.salesService.getPendingOrders();
    this.hasCartItems = this.salesService.getCartItems().length !== 0;

    if (!this.viewMode && this.orders.length === 0) {
      setTimeout(() => this.newOrder(), 500);
    }
  }

  async newOrder() {
    const alert = await this.alertCtrl.create({
      header: "New Order",
      subHeader: `${this.salesService.getCartItems().length} item(s)`,
      mode: "ios",
      cssClass: 'new-transaction',
      backdropDismiss: false,
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: "Enter reference name"          
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Continue',
          handler: async (data) => {
            if (data && data.name) this.addPendingOrder(data.name)
          }
        }        
      ]
    });

    alert.present();
  }

  async addPendingOrder(name: string) {
    this.salesService.addPendingOrder(name);
    this.getPendingOrders();
  }

  removePendingOrder(id) {
    this.salesService.removePendingOrder(id);
    this.getPendingOrders();
  }

  async openOrder(order) {

    const modal = await this.modalCtrl.create({
      component: PendingOrderComponent,
      componentProps: {
        id: order.id
      }
    });

    modal.onDidDismiss().then(res => {
      if (res.data) this.getPendingOrders();
    })

    modal.present();
  }
}
