import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { SalesService } from 'src/app/services/sales.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pending-order',
  templateUrl: './pending-order.component.html',
  styleUrls: ['./pending-order.component.scss'],
})
export class PendingOrderComponent implements OnInit {  
  order: any;
  cartItems = [];
  hasDeleted: boolean;
  viewMode: boolean;

  constructor(private params: NavParams, private route: ActivatedRoute, private router: Router, private salesService: SalesService, private modal: ModalController) { }

  ngOnInit() {
    const id = this.params.get('id');
    this.order = this.salesService.getPendingOrders().find(x => x.id == id);
    
    this.viewMode = this.route.snapshot.queryParams.viewMode !== undefined;
    if (!this.viewMode) this.cartItems = this.salesService.getCartItems();
  }

  async back(data?: any) {    
    await this.modal.dismiss(data || this.hasDeleted);
  }

  addPendingOrderItems() {
    this.salesService.addPendingOrderItems(this.order.id);
    this.back(true);
  }

  removePendingOrderItem(uid) {
    this.order = this.salesService.removePendingOrderItem(this.order.id, uid);    
    this.hasDeleted = true;
  }
  
  async removeCartItem(item) {
    const index = this.cartItems.indexOf(this.cartItems.find(x => x.id == item.id));
    if (index > -1) {
      this.cartItems.splice(index, 1);
      
      this.salesService.setCartItems(this.cartItems);
    }
  }

  async settle() {
    await this.back();
    this.router.navigateByUrl('settle?id=' + this.order.id);
  }
}
