import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestoredPage } from './restored.page';

describe('RestoredPage', () => {
  let component: RestoredPage;
  let fixture: ComponentFixture<RestoredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestoredPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestoredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
