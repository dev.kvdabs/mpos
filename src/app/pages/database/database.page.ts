import { Component, OnInit } from '@angular/core';
import { SqliteDbCopy } from '@ionic-native/sqlite-db-copy/ngx';
import { File } from '@ionic-native/file/ngx';
import { ToastController, AlertController, LoadingController, NavController } from '@ionic/angular';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-database',
  templateUrl: './database.page.html',
  styleUrls: ['./database.page.scss'],
})
export class DatabasePage implements OnInit {
  dbBackupDate: number;

  constructor(private sqliteDbCopy: SqliteDbCopy, 
              private settings: SettingsService,
              private loading: LoadingController, 
              private file: File, 
              private alertCtrl: AlertController, 
              private nav: NavController,
              private toastCtrl: ToastController) { }

  ngOnInit() {
    this.dbBackupDate = this.settings.dbBackupDate;
  }

  async backup() {

    const loader = await this.loading.create({
      backdropDismiss: false,
      mode: 'ios',
      message: 'Copying database to internal storage'
    });

    loader.present();

    try {
      
      await this.sqliteDbCopy.copyDbToStorage("data.db", 0, this.file.externalRootDirectory + "/saleskeep/", true);

      this.dbBackupDate = await this.settings.lastBackupDate();

      const toast = await this.toastCtrl.create({
        mode: 'ios', 
        color: 'success',
        message: 'Database successfully backed up.',
        duration: 3000
      });

      toast.present();



    } catch (error) {
      const toast = await this.alertCtrl.create({
        mode: 'ios',        
        message: 'Backup failed: ' + error.message,
        backdropDismiss: false,
        buttons: [
          {
            text: 'OK'
          }
        ]
      });

      toast.present();
    } finally {
      if (loader) loader.dismiss();
    }
    
  }

  async restore() {

    const alert = await this.alertCtrl.create({
      mode: 'ios',        
      message: 'Are you sure you want to restore database from backup?',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Yes, Restore',
          handler: () => {
            this.doRestore();
          }
        }
      ]
    });
    
    alert.present();
  }

  async doRestore() {
    const loader = await this.loading.create({
      backdropDismiss: false,
      mode: 'ios',
      message: 'Restoring database from internal storage'
    });

    loader.present();

    try {

      this.nav.navigateRoot('restored');

    } catch (error) {
      const toast = await this.alertCtrl.create({
        mode: 'ios',        
        message: 'Restore failed: ' + error.message,
        backdropDismiss: false,
        buttons: [
          {
            text: 'OK'
          }
        ]
      });

      toast.present();
    } finally {
      if (loader) loader.dismiss()
    }
    
  }
}
