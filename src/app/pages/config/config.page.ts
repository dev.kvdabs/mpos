import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss'],
})
export class ConfigPage implements OnInit {
  payLater: boolean;

  constructor(private settingsService: SettingsService) { }

  ngOnInit() {
    this.payLater = this.settingsService.payLater;
  }

  ionViewWillLeave() {
    this.settingsService.updatePayLater(this.payLater);
  }
}
