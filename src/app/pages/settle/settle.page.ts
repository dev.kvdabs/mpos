import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { SalesService } from 'src/app/services/sales.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-settle',
  templateUrl: './settle.page.html',
  styleUrls: ['./settle.page.scss'],
})
export class SettlePage implements OnInit {
  
  showFooter: boolean;

  cart = {
    id: 0,
    total: 0,
    discount: 0,
    net: 0,
    items: []
  };

  cash: any;
  change = 0;

  bills = [50, 100, 500, 1000];
  keypads = [7,8,9,4,5,6,1,2,3,'C',0,'.'];

  constructor(private nav: NavController, 
              private cdr: ChangeDetectorRef,
              private toastCtrl: ToastController,
              private salesService: SalesService,
              private route: ActivatedRoute,
              private loadingCtrl: LoadingController) { }

  ngOnInit() { 

    this.cart.id = parseInt(this.route.snapshot.queryParams.id);
    
    this.cartSummary();

    this.salesService.$discount.subscribe(discount => {
      this.cart.discount = discount;
      this.computeNet();
    });

    setTimeout(() => this.showFooter = true, 500);
  }

  cartSummary() {

    if (!this.cart.id) this.cart.items = this.salesService.getCartItems();
    else {
      const cart = this.salesService.getPendingOrders().find(x => x.id == this.cart.id);
      if (cart) {
        this.cart.items = cart.cart_items;
      }
    }    
    
    const total = this.cart.items.reduce( (tot, record) => {
        return tot + record.amount;
    }, 0);

    this.cart.total = total;
    this.cart.net = total;

    this.cash = total;
  }

  discount() {
    this.nav.navigateForward('discount?total=' + this.cart.total);
  }

  computeNet() {
    this.cart.net = this.cart.total - this.cart.discount;
    this.computeChange();

    setTimeout(() => this.cdr.detectChanges(), 500);
  }

  computeChange() {
    const cash = parseFloat(parseFloat((this.cash || 0)).toFixed(2));
    this.change = cash - this.cart.net;
  }
  
  async removeItem(item) {
    const index = this.cart.items.indexOf(this.cart.items.find(x => x.id == item.id));
    if (index > -1) {
      this.cart.items.splice(index, 1);
      
      this.salesService.setCartItems(this.cart.items);

      this.cartSummary();
      this.computeNet();
    }
  }

  keypress(key) {
    const cash = this.cash || 0;

    if (key == 'C') this.cash = 0;
    else if (key == '.' && cash.toString().split('.').length > 1) return;
    else {
      this.cash = cash.toString() + key;
    }

    this.computeChange();
  }

  bill(amount) {
    this.cash = amount;
    this.computeChange();
  }

  color(key): string {
    //if (key == "CLR" || key == ".") return "light";
    return "dark";
  }

  async settle() {

    const alert = await this.loadingCtrl.create({
      mode: 'ios',
      message: 'Saving transaction...'
    });

    alert.present();

    try {

      if (this.change < 0) return;
     
      await this.salesService.add(this.cart);
      
      setTimeout(() => {
        
        if (alert) alert.dismiss();

        this.salesService.clearCartItems();
        
        this.nav.navigateRoot("sales");

      }, 500);

    } catch (error) {
      if (alert) alert.dismiss();

      const toast = await this.toastCtrl.create({
        color: 'danger',
        mode: 'ios',
        message: 'Oops! Something went wrong. Please contact support.'
      });
      toast.present();

    }

  }
}
