import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SalesService } from 'src/app/services/sales.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.page.html',
  styleUrls: ['./discount.page.scss'],
})
export class DiscountPage implements OnInit {

  total: number;
  discounts = [3, 5, 7, 10, 15, 20, 30, 40, 50];
  discountAmount: any = 0;
  selected: number;

  keypads = [7,8,9,4,5,6,1,2,3,'C',0,'.'];

  constructor(private route: ActivatedRoute, private nav: NavController, private salesService: SalesService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.total = params.total;
    });
  }

  ionViewWillLeave() {
    let discount = parseFloat(this.discountAmount || 0);

    this.salesService.$discount.next(parseFloat(discount.toFixed(2)));
  }

  calcDiscount(discount) {
    this.selected = discount;
    this.discountAmount = parseFloat((this.total * (discount / 100)).toFixed(2));
  }

  addDiscount() {
    this.salesService.$discount.next(this.discountAmount);
    this.nav.back();
  }

  keypress(key) {
    const discountAmount = this.discountAmount || '';

    if (key == 'C') return this.discountAmount = 0;

    if (key == '.' && discountAmount.toString().split('.').length > 1) return;
    else {
      this.discountAmount = discountAmount.toString() + key;
    }    
  }
}
