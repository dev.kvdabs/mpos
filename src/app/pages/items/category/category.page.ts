import { Component, OnInit } from '@angular/core';
import { ItemService } from 'src/app/services/item.service';
import { Category, Item } from 'src/app/models/item.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  category: Category;
  items = [];

  $item: Subscription;

  constructor(private service: ItemService, private router: Router) { }

  ngOnInit() {
    this.category = this.service._category;
    this.getItems();

    this.$item = this.service.$item.subscribe(() => this.getItems());
  }

  ngOnDestroy() {
    if (this.$item) this.$item.unsubscribe();
  }

  getItems() {
    this.service.getItems(this.category.id, false).then(items => {
      this.items = [...items];
    });
  }

  add() {

    this.service._item = {
      categoryId: this.category.id
    };

    this.openItem();
  }

  edit(item: Item) {
    this.service._item = item;
    this.openItem();
  }

  openItem() {
    this.router.navigateByUrl('item');
  }

  ionViewWillLeave() {
    if (this.category.name) {
      this.service.updateCategory(this.category);
    }
  }
}
