import { Component, OnInit } from '@angular/core';
import { ItemService } from 'src/app/services/item.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Item } from 'src/app/models/item.model';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-item',
  templateUrl: './item.page.html',
  styleUrls: ['./item.page.scss'],
})
export class ItemPage implements OnInit {

  form: FormGroup

  item: Partial<Item>;

  constructor(private service: ItemService, 
              private fb: FormBuilder, 
              private toastCtrl: ToastController,
              private nav: NavController) { }

  ngOnInit() {
    this.item = this.service._item;

    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      id: [''],
      categoryId: [this.item.categoryId, Validators.required],
      code: [''],
      name: ['', Validators.required],
      uom: [''],
      cost: [''],
      price: ['', Validators.required],
      isActive: [true]
    });

    for (var key in this.item) {
      this.form.controls[key].setValue(this.item[key])
    }
  }

  async save() {
    try {
      if (!this.form.valid) return;

      const item = this.form.value;

      if (!item.cost) item.cost = 0;

      const res = await this.service.addUpdateItem(item);
      if (res.success) {

        this.nav.back();

      } else {

        (await this.toastCtrl.create({
          message: res.message,
          mode: "ios",
          color: "danger",
          duration: 15000,
          showCloseButton: true
        })).present();

      }


    } catch (error) {
      
    }

  }

}
