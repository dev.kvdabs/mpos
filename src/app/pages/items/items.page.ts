import { Component, OnInit } from '@angular/core';
import { ItemService } from 'src/app/services/item.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Category } from 'src/app/models/item.model';

@Component({
  selector: 'app-items',
  templateUrl: './items.page.html',
  styleUrls: ['./items.page.scss'],
})
export class ItemsPage implements OnInit {
  categories = [];
  items = [];
  
  constructor(private service: ItemService, private router: Router, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.getCategories();

    this.service.$item.subscribe((item) => {

      const index = this.items.indexOf(this.items.find(x => x.id == item.id));
      if (index > -1) this.items[index] = item;

    });
  }

  getCategories() {
    this.service.getCategories(false).then(categories => {
      this.categories = [...categories];
    });
  }

  async add() {
    const alert = await this.alertCtrl.create({
      header: 'New Category',
      mode: "md",
      cssClass: 'buy',
      backdropDismiss: false,
      inputs: [
        {
          name: 'category',
          type: 'text',
          placeholder: "Enter category name"          
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Save',
          handler: async (data) => {

            if (data.category && data.category.trim()) {
              this.addCategory(data);
            }

          }
        }        
      ]
    });

    alert.present();
  }

  addCategory(data) {
    this.service.addCategory(data.category.trim().toUpperCase()).then(() => {
      this.getCategories();      
    });
  }

  showItems(category: Category) {
    this.service._category = category;
    this.router.navigateByUrl('category');
  }

  async search(event) {
    if (event) {
      const searchkey = event.target.value;
      this.items = searchkey ? await this.service.searchItems(searchkey) : [];  
    }
  }

  showItem(item) {
    this.service._item = item;
    this.router.navigateByUrl('item');
  }
}
