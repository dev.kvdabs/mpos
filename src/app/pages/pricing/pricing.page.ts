import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.page.html',
  styleUrls: ['./pricing.page.scss'],
})
export class PricingPage implements OnInit {

  deviceUid = '';
  clientCode = '';
  clientName = '';
  expiry: number;
  
  constructor(private settingsService: SettingsService, private uniqueDeviceID: UniqueDeviceID, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private httpClient: HttpClient, private toastCtrl: ToastController) { }

  ngOnInit() {
    this.clientCode = this.settingsService.clientCode;
    this.clientName = this.settingsService.clientName;
    this.expiry = this.settingsService.expiry;

    this.uniqueDeviceID.get().then(uuid => this.deviceUid = uuid);
  }

  async enterClientCode() {
    const alert = await this.alertCtrl.create({
      header: 'Enter client code',      
      mode: "ios",
      cssClass: 'buy',
      backdropDismiss: false,
      inputs: [
        {
          name: 'code',
          type: 'text',
          value: this.clientCode,
          placeholder: "1"
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Renew',
          handler: async (data) => {

            if (data.code) this.renew(data.code);

          }
        }        
      ]
    });

    alert.present();
  }

  async renew(code: string) {
    //http://112.198.195.29:52875/api/salesman/auth


    try {

      const headers = new HttpHeaders({
        //'Accept': 'application/json',
        'Authorization':'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ7XCJVc2VyR3JvdXBJRFwiOm51bGwsXCJVc2VybmFtZVwiOlwiYWRtaW5cIixcIlBhc3N3b3JkXCI6XCJhZG1pbjEyM1wiLFwiRGlzcGxheU5hbWVcIjpcIlByb2dyYW1tZXJcIixcIkRlc2NyaXB0aW9uXCI6bnVsbCxcIkltYWdlXCI6bnVsbCxcIkVtcGxveWVlSURcIjoxLFwiSXNQcm9ncmFtbWVyQWNjb3VudFwiOnRydWUsXCJBZGRpdGlvbmFsUmlnaHRzXCI6bnVsbCxcIlVzZXJHcm91cFwiOm51bGwsXCJFbXBsb3llZVwiOntcIkZpcnN0TmFtZVwiOlwiMTAyOFwiLFwiTWlkZGxlTmFtZVwiOm51bGwsXCJMYXN0TmFtZVwiOlwiUHJvZ3JhbW1lclwiLFwiUG9zaXRpb25cIjpudWxsLFwiR2VuZGVyXCI6bnVsbCxcIkJpcnRoRGF0ZVwiOlwiMDAwMS0wMS0wMVQwMDowMDowMFwiLFwiQmlydGhQbGFjZVwiOm51bGwsXCJDaXZpbFN0YXR1c1wiOm51bGwsXCJOYXRpb25hbGl0eVwiOlwiRmlsaXBpbm9cIixcIlJlbGlnaW9uXCI6bnVsbCxcIlByZXNlbnRBZGRyZXNzXCI6bnVsbCxcIlBlcm1hbmVudEFkZHJlc3NcIjpudWxsLFwiQ29udGFjdE5vXCI6bnVsbCxcIk1vYmlsZU5vXCI6bnVsbCxcIkRlcGFydG1lbnRJRFwiOjEsXCJEZXBhcnRtZW50XCI6bnVsbCxcIkZ1bGxuYW1lXCI6XCIxMDI4ICBQcm9ncmFtbWVyXCIsXCJBZ2VcIjoyMDE5LFwiSURcIjoxLFwiU0FQSWRcIjpudWxsLFwiQ3JlYXRlZE9uXCI6XCIyMDE5LTA5LTA2VDEyOjE4OjE3LjA1NTIwODRcIixcIkNyZWF0ZWRCeVwiOlwiXCIsXCJMYXN0TW9kaWZpZWRPblwiOlwiMjAxOS0wOS0wNlQxMjoxODoxNy4wNTUyMDk4XCIsXCJMYXN0TW9kaWZpZWRCeVwiOlwiXCIsXCJSZW1hcmtzXCI6XCJcIixcIlJlY29yZFN0YXR1c1wiOjB9LFwiSURcIjoxLFwiU0FQSWRcIjpudWxsLFwiQ3JlYXRlZE9uXCI6XCIyMDE5LTA5LTA2VDEyOjE4OjE3LjE2MjYzMVwiLFwiQ3JlYXRlZEJ5XCI6XCJcIixcIkxhc3RNb2RpZmllZE9uXCI6XCIyMDE5LTA5LTA2VDEyOjE4OjE3LjE2MjYzMjFcIixcIkxhc3RNb2RpZmllZEJ5XCI6XCJcIixcIlJlbWFya3NcIjpcIlwiLFwiUmVjb3JkU3RhdHVzXCI6MH0iLCJqdGkiOiIwNTNhMGE4MS03NDY4LTRjNWMtYjJiOS0zNGE0MWM5N2U0MzAiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjEiLCJleHAiOjE1ODI5ODI5MjQsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NTI4NzUvIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo1Mjg3NS8ifQ.nL73RBxr-xjbSXYm3iOeYl8HThyormeYHyOBBztpDLo'
      });

      this.httpClient.get("http://112.198.195.29:52875/api/salesman/orders", { headers: headers}).subscribe(res => {
        console.log(res)
      })

    } catch (error) {
      console.error(error)
    }

    // const loader = await this.loadingCtrl.create({
    //   mode: "ios",
    //   message: "Please wait..."
    // });

    // try {

    //   loader.present();

    //   const data: any = await this.httpClient.get(`https://us-central1-litetouch-2aed2.cloudfunctions.net/validateSubscription?clientCode=${code}&deviceUid=${this.deviceUid}`).toPromise();

    //   if (data.success) {

    //     await this.settingsService.renew(data);
        
    //     this.ngOnInit();

    //     setTimeout(async () => {
    //       if (loader) loader.dismiss();
          
    //       const toast = await this.toastCtrl.create({
    //         message: "Subscription successfully renewed",
    //         color: 'success',
    //         mode: 'ios',
    //         duration: 2000
    //       });

    //       toast.present();        
    //     }, 2000);

    //   } else {

    //     setTimeout(async () => {
    //       if (loader) loader.dismiss();
    //       const toast = await this.toastCtrl.create({
    //         message: 'FAILED. ' + (data.message || "Invalid renewal code"), 
    //         mode: 'ios',
    //         color: 'danger',
    //         duration: 10000
    //       });

    //       toast.present();       
    //     }, 2000);
        
    //   }

    // } catch (error) {
    //   if (loader) loader.dismiss();
    // } finally {
      
    // }
  }
}
