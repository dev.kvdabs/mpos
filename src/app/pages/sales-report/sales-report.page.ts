import { Component, OnInit, ViewChild } from '@angular/core';
import { SalesService } from 'src/app/services/sales.service';
import { SettingsService } from 'src/app/services/settings.service';
import { NavController } from '@ionic/angular';
import { Chart } from 'chart.js';
import { Subscribable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.page.html',
  styleUrls: ['./sales-report.page.scss'],
})
export class SalesReportPage implements OnInit {
  @ViewChild('barChart', null) barChart: any;

  bars: any;
  colorArray: any;

  today: any = {};
  display = "1";
  reportType = 'today';

  months = [
    { id: 1, month: 'January'},
    { id: 2, month: 'February'},
    { id: 3, month: 'March'},
    { id: 4, month: 'April'},
    { id: 5, month: 'May'},
    { id: 6, month: 'June'},
    { id: 7, month: 'July'},
    { id: 8, month: 'August'},
    { id: 9, month: 'September'},
    { id: 10, month: 'October'},
    { id: 11, month: 'November'},
    { id: 12, month: 'December'}
  ];
  years = [];

  now: Date;
  month: number;
  year: number;
  daily = [];
  monthly = [];
  voided = [];

  items = [];

  $receiptCancelledSub: Subscription;

  constructor(private salesService: SalesService, private nav: NavController, private router: Router) { }

  async ngOnInit() {

    for (var i=2019; i<=2030; i++) {
      this.years.push({
        id: i,
        year: i
      })
    }

    this.today = await this.salesService.todaySalesReport();
    this.getToday();

    this.now = new Date();
    this.month = new Date().getMonth() + 1;
    this.year = new Date().getFullYear();

    this.$receiptCancelledSub = this.salesService.$receiptCancelled.subscribe(async () => {
      this.today = await this.salesService.todaySalesReport();
      this.getToday();
    })
  }

  ngOnDestroy() {
    if (this.$receiptCancelledSub) this.$receiptCancelledSub.unsubscribe();
  }

  async selectReport() {
    this.display = "1";

    switch (this.reportType) {
      case "today":
        this.getToday();
        break;
      case "daily":
        this.getDailyReport();
        break;
      case "monthly":
        this.getMonthlyReport();
        break;
    }
    
  }

  selectDisplay() {
    if (this.reportType == "today" && this.display == "3") this.getVoidedSales();
    else if (this.reportType == "daily" && this.display == "3") this.getVoidedSales(this.month);
  }

  async getToday() {
    const now = new Date();

    this.salesService.ItemSummarySalesReport(now.getMonth() + 1, now.getDate()).then(data => {
      this.items = data;
    });
  }

  async getDailyReport() {
    this.daily = await this.salesService.dailySalesReport(this.month);
    this.salesService.ItemSummarySalesReport(this.month).then(data => {
      this.items = data;
    });
  }

  async getMonthlyReport() {
    this.monthly = await this.salesService.monthlySalesReport(this.year);  
    this.createBarChart();      
  }

  perInvoiceSalesReport(date: Date) {
    this.salesService.salesDate = date;
    this.nav.navigateForward('per-invoice');
  }

  viewTodaysReceipts() {
    this.perInvoiceSalesReport(new Date());
  }

  async getVoidedSales(month?: number) {
    this.voided = [];    
    this.voided = await this.salesService.getVoidedSales(month);
    console.log(this.voided)
  }

  details(sale) {
    this.router.navigateByUrl('per-invoice/details?sale=' + JSON.stringify(sale));
  }

  getMonth(month: number, full: boolean = false) {
    const m = this.months.find(m => m.id === month);
    if (full) return m.month;
    return m.month.substr(0, 3);
  }

  createBarChart() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: this.monthly.map(x => this.getMonth(x.month)),
        datasets: [{
          label: this.year.toString() + " Net Sales",
          data: this.monthly.map(x => x.net),
          backgroundColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(38, 194, 129)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
}
