import { Component, OnInit } from '@angular/core';
import { SalesService } from 'src/app/services/sales.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-per-invoice',
  templateUrl: './per-invoice.page.html',
  styleUrls: ['./per-invoice.page.scss'],
})
export class PerInvoicePage implements OnInit {
  date: Date;
  sales = [];

  $receiptCancelledSub: Subscription;

  constructor(private salesService: SalesService, private router: Router) { }

  async ngOnInit() {
    this.date = this.salesService.salesDate;    
    this.sales = await this.salesService.perInvoiceSalesReport();

    this.$receiptCancelledSub = this.salesService.$receiptCancelled.subscribe(async () => {
      this.sales = await this.salesService.perInvoiceSalesReport();
    })
  }

  ngOnDestroy() {
    if (this.$receiptCancelledSub) this.$receiptCancelledSub.unsubscribe();
  }

  details(sale) {
    this.router.navigateByUrl('per-invoice/details?sale=' + JSON.stringify(sale));
  }

  
}
