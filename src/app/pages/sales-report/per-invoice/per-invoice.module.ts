import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PerInvoicePage } from './per-invoice.page';
import { SalesDetailsComponent } from './sales-details/sales-details.component';

const routes: Routes = [
  {
    path: '',
    component: PerInvoicePage
  },
  {
    path: 'details',
    component: SalesDetailsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PerInvoicePage, SalesDetailsComponent]
})
export class PerInvoicePageModule {}
