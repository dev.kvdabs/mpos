import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerInvoicePage } from './per-invoice.page';

describe('PerInvoicePage', () => {
  let component: PerInvoicePage;
  let fixture: ComponentFixture<PerInvoicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerInvoicePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
