import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesService } from 'src/app/services/sales.service';
import { LoadingController, NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-sales-details',
  templateUrl: './sales-details.component.html',
  styleUrls: ['./sales-details.component.scss'],
})
export class SalesDetailsComponent implements OnInit {

  sale: any;
  saleItems = [];

  constructor(private activatedRoute: ActivatedRoute, private nav: NavController, private service: SalesService, private loadingCtrl: LoadingController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(param => {
      try {
        this.sale = JSON.parse(param.sale);
        this.service.getSalesItems(this.sale.id).then(items => this.saleItems = items);
      } catch (error) {
        alert(JSON.stringify(error))
      }
    })
  }

  async void() {
    const alert = await this.alertCtrl.create({
      mode: 'ios',
      header: 'Void Receipt',
      message: 'This action cannot be undone.',
      backdropDismiss: false,
      inputs: [
        {
          name: 'remarks',
          type: 'text',
          placeholder: "Enter remarks here..." 
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Void',
          handler: async (data) => {
            this.voidReceipt(data.remarks);
          }
        }        
      ]
    });

    alert.present();
  }

  async voidReceipt(remarks: string) {
    const alert = await this.loadingCtrl.create({
      mode: 'ios',
      message: 'Voiding receipt...'
    });

    alert.present();

    try {
      await this.service.voidReceipt(this.sale.id, remarks);

      setTimeout(() => {
        if (alert) alert.dismiss();
        this.nav.pop();
      }, 500);

    } catch (error) {
      
      if (alert) alert.dismiss();

    }
  }
}
