import { Component } from '@angular/core';
import { SettingsService } from '../services/settings.service';
import { NavController } from '@ionic/angular';
import { DbService } from '../services/db/db.service';

declare var window: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private db: DbService, private settingsService: SettingsService, private nav: NavController) {
    
  }

  async ngOnInit() {
    this.db.getDatabaseState().subscribe(async (ready) => {
      if (ready) {
        try {
          
          await this.settingsService.loadSettings();
          this.nav.navigateRoot("sales");

        } catch (error) {
          alert(JSON.stringify(error))
        }
      }
    }, error => alert(JSON.stringify(error)));
  }
}
