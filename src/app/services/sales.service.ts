import { Injectable } from '@angular/core';
import { DbService } from './db/db.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  $clearCart = new Subject<boolean>();
  $cart = new Subject<any>();
  $pendingOrder = new Subject<number>();

  CART_KEY = 'cart_items';
  PENDING_ORDERS_KEY = 'pending_orders';

  $discount = new Subject<number>();
  
  salesDate: Date;

  $receiptCancelled = new Subject<any>();

  constructor(private db: DbService) { 
    
  }

  addToCart(item) {

    let items = this.getCartItems();
    items.push(item);

    localStorage.setItem(this.CART_KEY, JSON.stringify(items));
  }

  getPendingOrders(): any[] {
    let _orders = localStorage.getItem(this.PENDING_ORDERS_KEY);
    if (_orders) return JSON.parse(_orders);

    return [];
  }

  pendingOrdersCount() {
    const count = this.getPendingOrders().length;
    this.$pendingOrder.next(count);
  }

  addPendingOrder(name: string) {
    let orders = this.getPendingOrders();

    const cartItems = JSON.parse(localStorage.getItem(this.CART_KEY));

    const total = cartItems.reduce( (tot, record) => {
      return tot + record.amount;
    }, 0);

    orders.push({
      id: new Date().getTime(),
      name: name,
      quantity: cartItems.length,
      total: total,
      cart_items: this.getCartItems()
    });
    
    localStorage.setItem(this.PENDING_ORDERS_KEY, JSON.stringify(orders));

    this.$pendingOrder.next(orders.length);
    this.clearCartItems();
  }

  removePendingOrder(id) {
    let orders = this.getPendingOrders().filter(x => x.id !== id);
    localStorage.setItem(this.PENDING_ORDERS_KEY, JSON.stringify(orders));
    this.$pendingOrder.next(orders.length);
  }

  addPendingOrderItems(id) {
    let orders = this.getPendingOrders();
    let order = orders.find(x => x.id == id);

    const cartItems = this.getCartItems();
    order.cart_items.push(...cartItems);
    order.quantity = order.cart_items.length;
    order.total = this.computeTotal(order.cart_items);
    
    localStorage.setItem(this.PENDING_ORDERS_KEY, JSON.stringify(orders));

    this.clearCartItems();
  }

  removePendingOrderItem(orderId, uid) {
    let orders = this.getPendingOrders();
    let order = orders.find(x => x.id == orderId);
    order.cart_items = order.cart_items.filter(x => x.uid !== uid);
    order.quantity = order.cart_items.length;
    order.total = this.computeTotal(order.cart_items);

    localStorage.setItem(this.PENDING_ORDERS_KEY, JSON.stringify(orders));

    return order;
  }

  getCartItems(): any[] {
    let _items = localStorage.getItem(this.CART_KEY);
    if (_items) return JSON.parse(_items);

    return [];
  }

  setCartItems(items) {
    localStorage.setItem(this.CART_KEY, JSON.stringify(items));
    this.$cart.next();
  }

  clearCartItems() {
    localStorage.removeItem(this.CART_KEY);
    this.$clearCart.next(true);
  }

  computeTotal(items) {
    return items.reduce( (tot, record) => {
      return tot + record.amount;
    }, 0);
  }

  async add(cart) {
    
    try {
      
      let sql = `INSERT OR IGNORE INTO Sales(salesDate, year, month, day, total, discount, net) VALUES(${new Date().getTime()}, ${new Date().getFullYear()}, ${new Date().getMonth() + 1}, ${new Date().getDate()}, ${cart.total}, ${cart.discount}, ${cart.net})`;
      
      const sale = await this.db.executeSql(sql);

      let promises = [];

      cart.items.forEach(item => {
        sql = `INSERT OR IGNORE INTO SalesItem(salesId, itemId, name, quantity, price, amount) VALUES(${sale.insertId}, ${item.id}, '${item.name}', ${item.quantity}, ${item.price}, ${item.amount})`;        
        promises.push(this.db.executeSql(sql));
      });

      await Promise.all(promises);

      this.removePendingOrder(cart.id);

    } catch (error) {
      console.log(error)
    }

  }

  async todaySalesReport() {
    let from = new Date();
    from.setHours(0);
    from.setMinutes(0);
    from.setSeconds(0);
    from.setMilliseconds(0);

    let to = new Date();
    to.setHours(23);
    to.setMinutes(59);
    to.setSeconds(59);
    to.setMilliseconds(0);


    let sql = `SELECT COUNT(id) as count, SUM(total) as total, SUM(discount) as discount, SUM(net) as net FROM Sales WHERE cancelled = 0 AND salesDate >= ${from.getTime()} AND salesDate <= ${to.getTime()} LIMIT 1`;
    const results = await this.db.executeSql(sql);

    return results.rows.item(0);
  }

  async dailySalesReport(month: number) {
    try {
      
      let sql = `SELECT COUNT(id) as count, salesDate, day, SUM(total) as total, SUM(discount) as discount, SUM(net) as net FROM Sales`;
      sql += ` WHERE cancelled = 0 AND year=${new Date().getFullYear()} AND month=${month}`;
      sql += ` GROUP BY day`;

      const results = await this.db.executeSql(sql);

      return this.db.formatResults(results);

    } catch (error) {
      alert(JSON.stringify(error))

      return [];
    }
  }

  async monthlySalesReport(year: number) {
    try {
      
      let sql = `SELECT COUNT(id) as count, month, SUM(total) as total, SUM(discount) as discount, SUM(net) as net FROM Sales`;
      sql += ` WHERE cancelled = 0 AND year=${year}`;
      sql += ` GROUP BY month`;


      const results = await this.db.executeSql(sql);

      return this.db.formatResults(results);

    } catch (error) {
      alert(JSON.stringify(error))

      return [];
    }
  }

  async perInvoiceSalesReport() {
    try {
      
      let sql = `SELECT * FROM Sales`;
      sql += ` WHERE cancelled = 0 AND year=${new Date(this.salesDate).getFullYear()} AND month=${new Date(this.salesDate).getMonth() + 1} AND day=${new Date(this.salesDate).getDate()}`;
      sql += ` ORDER BY salesDate DESC`;
      
      const results = await this.db.executeSql(sql);
      return this.db.formatResults(results);

    } catch (error) {
      console.log(error)

      return [];
    }
  }

  async ItemSummarySalesReport(month: number, day?: number) {
    try {
      
      let sql = `SELECT itemId, Item.name, SUM(quantity) as count from Sales
                    INNER JOIN SalesItem on SalesItem.salesId = Sales.id 
                    INNER JOIN Item ON Item.id = SalesItem.itemId
                  WHERE Sales.cancelled = 0 AND Sales.year = ${new Date().getFullYear()} AND Sales.month = ${month} ${day ? 'AND Sales.day = ' + day : ''}
                  GROUP BY itemId ORDER BY count desc`;
      
      const results = await this.db.executeSql(sql);
      return this.db.formatResults(results);

    } catch (error) {
      console.log(error)

      return [];
    }
  }

  async getSalesItems(salesId: number) {
    try {
      let sql = `SELECT * FROM SalesItem WHERE salesId = ${salesId}`;
      const results = await this.db.executeSql(sql);
      return this.db.formatResults(results);
    } catch (error) {
      console.log(error)
      return [];
    }
  }

  async voidReceipt(id: number, remarks: string, cancel: boolean = true) {
    try {
      let sql = `UPDATE Sales SET cancelled=${cancel} WHERE id=${id}`;
      if (remarks) {
        sql = `UPDATE Sales SET cancelled=${cancel}, remarks='${remarks}' WHERE id=${id}`;
      }      
      
      console.log(sql)
      await this.db.executeSql(sql);

      this.$receiptCancelled.next();

      return true;

    } catch (error) {
      return false;
    }
  }

  async getVoidedSales(month?: number) {
    try {
      
      let sql = '';
      sql = `SELECT * FROM Sales WHERE cancelled = 1`;

      const today = new Date();

      if (!month) {      
        sql += ` AND year=${today.getFullYear()} AND month=${today.getMonth()+1} AND day=${today.getDate()}`
      } else {
        sql += ` AND year=${today.getFullYear()} AND month=${month}`
      }

      sql += ` ORDER BY salesDate DESC`

      const results = await this.db.executeSql(sql);
      return this.db.formatResults(results);
      
    } catch (error) {
      return [];
    }
  }
}
