import { Injectable } from '@angular/core';
import { DbService } from './db/db.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  
  license: string;
  expiry: number;
  expired: boolean;
  clientCode: string;
  clientName: string;
  payLater: boolean;
  dbBackupDate: number;

  constructor(private db: DbService) { }

  async loadSettings() {
    try {
      let sql = '';

      let today = new Date();
      let days = today.getDate() + 15;
      let expiry = new Date().setDate(days);

      sql = `INSERT OR IGNORE INTO Settings(id, clientCode, name, license, expiry) values(1, 'FREE_TRIAL', 'FREE TRIAL', 'PRO', ${expiry})`    
      
      await this.db.executeSql(sql);

      sql = `SELECT * FROM Settings LIMIT 1`;    
      const results = await this.db.executeSql(sql);

      const settings = results.rows.item(0);

      this.clientName = settings.name;
      this.clientCode = settings.clientCode;
      this.license = settings.license;      
      this.expiry = settings.expiry;
      this.expired = this.expiry < new Date().getTime();
      this.payLater = settings.payLater;
      this.dbBackupDate = settings.dbBackupDate;

    } catch (error) {
      alert('Error loadSettings: ' + JSON.stringify(error))
    }
  }

  async renew(client) {
    let sql = `UPDATE Settings set clientCode='${client.code}', name='${client.name}', expiry = ${client.expiry}, license = '${client.license}'`
    await this.db.executeSql(sql);
    await this.loadSettings();
  }

  async updatePayLater(enabled: boolean) {
    let sql = `UPDATE Settings set payLater= ${enabled}`;    
    await this.db.executeSql(sql);
    this.payLater = enabled
  }

  lastBackupDate(): number {
    const backupDate = new Date().getTime();
    let sql = `UPDATE Settings set dbBackupDate=${backupDate}`;    
    this.db.executeSql(sql);

    return backupDate;
  }
}
