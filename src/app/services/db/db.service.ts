import { Injectable } from '@angular/core';
import { dbInstance } from './dbInstance';
import { Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject } from 'rxjs';

declare var window: any;

@Injectable({
  providedIn: 'root'
})
export class DbService {
  private db: SQLiteObject | any;

  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private platform: Platform, private sqlite: SQLite) { 
    this.platform.ready().then(async () => {
      if (this.platform.is('cordova')) {

        try {
          this.db = await this.sqlite.create({
            name: 'data.db',
            location: 'default'
          });

          this.init();
          setTimeout(() => this.dbReady.next(true), 3000);
          
        } catch (error) {
          alert('error sqlite: ' + JSON.stringify(error))
        }
  
      } else {
  
        let db = window.openDatabase('__browser.db', '1.0', 'DEV', 5 * 1024 * 1024);
        this.db = dbInstance(db);
  
        this.init();        
        setTimeout(() => this.dbReady.next(true), 3000);
  
      } 
    })
  }

  loadDatabase() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db) => {
      
      this.db = db;
      
    });
  }

  async init() {
    
    let tables = [
      `CREATE TABLE IF NOT EXISTS Category(id INTEGER PRIMARY KEY AUTOINCREMENT)`,
      `CREATE TABLE IF NOT EXISTS Item(id INTEGER PRIMARY KEY AUTOINCREMENT)`,
      `CREATE TABLE IF NOT EXISTS Sales(id INTEGER PRIMARY KEY AUTOINCREMENT)`,      
      `CREATE TABLE IF NOT EXISTS SalesItem(id INTEGER PRIMARY KEY AUTOINCREMENT)`,
      `CREATE TABLE IF NOT EXISTS Settings(id INTEGER PRIMARY KEY AUTOINCREMENT)`
    ];

    let columns = [
      `ALTER TABLE Category ADD COLUMN name TEXT`,
      `ALTER TABLE Category ADD COLUMN isActive INTEGER default 1`,

      `ALTER TABLE Item ADD COLUMN categoryId INTEGER`,
      `ALTER TABLE Item ADD COLUMN code TEXT`,
      `ALTER TABLE Item ADD COLUMN name TEXT`,
      `ALTER TABLE Item ADD COLUMN uom TEXT`,
      `ALTER TABLE Item ADD COLUMN cost FLOAT`,
      `ALTER TABLE Item ADD COLUMN price FLOAT`,
      `ALTER TABLE Item ADD COLUMN isActive INTEGER default 1`,

      `ALTER TABLE Sales ADD COLUMN salesDate INTEGER`,
      `ALTER TABLE Sales ADD COLUMN year INTEGER`,
      `ALTER TABLE Sales ADD COLUMN month INTEGER`,
      `ALTER TABLE Sales ADD COLUMN day INTEGER`,
      `ALTER TABLE Sales ADD COLUMN total FLOAT`,
      `ALTER TABLE Sales ADD COLUMN discount FLOAT`,
      `ALTER TABLE Sales ADD COLUMN net FLOAT`,
      `ALTER TABLE Sales ADD COLUMN remarks TEXT`,
      `ALTER TABLE Sales ADD COLUMN cancelled INTEGER DEFAULT 0`,

      `ALTER TABLE SalesItem ADD COLUMN salesId INTEGER REFERENCES Sales(id)`,
      `ALTER TABLE SalesItem ADD COLUMN itemId INTEGER REFERENCES Item(id)`,
      `ALTER TABLE SalesItem ADD COLUMN name TEXT`,
      `ALTER TABLE SalesItem ADD COLUMN quantity FLOAT`,
      `ALTER TABLE SalesItem ADD COLUMN price FLOAT`,
      `ALTER TABLE SalesItem ADD COLUMN amount FLOAT`,

      `ALTER TABLE Settings ADD COLUMN name TEXT`,
      `ALTER TABLE Settings ADD COLUMN clientCode TEXT`,
      `ALTER TABLE Settings ADD COLUMN license TEXT`,
      `ALTER TABLE Settings ADD COLUMN expiry INTEGER`,
      `ALTER TABLE Settings ADD COLUMN payLater INTEGER DEFAULT 0`,
      `ALTER TABLE Settings ADD COLUMN dbBackupDate INTEGER`,
    ];

    let indexes = [
      `CREATE INDEX IF NOT EXISTS idx_category_id ON Item (categoryId)`,
      `CREATE INDEX IF NOT EXISTS idx_name ON Item (name)`,
      `CREATE INDEX IF NOT EXISTS idx_is_active ON Item (isActive)`,
      `CREATE INDEX IF NOT EXISTS idx_sales_date ON Sales (salesDate)`,
      `CREATE INDEX IF NOT EXISTS idx_year ON Sales (year)`,
      `CREATE INDEX IF NOT EXISTS idx_month ON Sales (month)`,
      `CREATE INDEX IF NOT EXISTS idx_day ON Sales (day)`,
      `CREATE INDEX IF NOT EXISTS idx_item_id ON SalesItem (itemId)`,
      `CREATE INDEX IF NOT EXISTS idx_quantity ON SalesItem (quantity)`
    ]

    let sqls = [...tables, ...columns, ...indexes]
    
    sqls.forEach(async (sql) => {
      this.executeSql(sql);
    });
  }

  getDatabase() {
    return this.db;
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  executeSql(sql: string): Promise<any> {    

    try {
      return this.db.executeSql(sql, []);

    } catch (error) {
      alert('executeSql: ' + JSON.stringify(error))
    }

  }

  formatResults(data: any) {
    let res = [];

    for (var i=0; i<data.rows.length; i++) {
      
      if (this.platform.is('cordova')) {

        res.push(data.rows.item(i));

      } else {

        res.push(data.rows[i]);
        
      }
    }
    
    return res;
  }
}
