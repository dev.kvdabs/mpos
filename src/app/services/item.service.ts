import { Injectable } from '@angular/core';
import { DbService } from './db/db.service';
import { Category, Item } from '../models/item.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  _category: Category;
  _item: Partial<Item>;

  $item = new Subject<any>();
  $category = new Subject<any>();

  constructor(private db: DbService) { }

  async getItems(categoryId: number, isActiveOnly: boolean = true) {
    try {
      let sql = `SELECT * FROM Item WHERE categoryId=${categoryId} ${isActiveOnly ? 'AND isActive=1' : ''} ORDER BY name ASC`;

      const data = await this.db.executeSql(sql);
    
      return this.db.formatResults(data);

    } catch (error) {
      
      return [];

    }
  }

  async searchItems(searchKey: string) {
    try {
      let sql = `SELECT * FROM Item WHERE name LIKE '%${searchKey}%' ORDER BY name ASC`;

      const data = await this.db.executeSql(sql);
    
      return this.db.formatResults(data);

    } catch (error) {
      
      return [];

    }
  }

  async getCategories(isActiveOnly: boolean = true): Promise<any[]> {
    try {
      let sql = `SELECT * FROM Category ${isActiveOnly ? 'WHERE isActive=1' : ''} ORDER BY name ASC`;

      const data = await this.db.executeSql(sql);
    
      return this.db.formatResults(data);

    } catch (error) {

      return [];

    }
  }

  async addCategory(category: string) {
    try {
      let sql = `INSERT OR IGNORE INTO Category(name) VALUES('${category.toUpperCase()}')`;

      await this.db.executeSql(sql);

      setTimeout(() => {
        this.$category.next();
      }, 500);

      return true;

    } catch (error) {
      console.log(error)
      return false;
    }
  }

  async updateCategory(category: Category) {
    try {
      let sql = `UPDATE Category SET name = '${category.name}', isActive = ${category.isActive} WHERE id = ${category.id}`;

      await this.db.executeSql(sql);

      setTimeout(() => {
        this.$category.next();
      }, 500);

      return true;

    } catch (error) {
      console.log(error)
      return false;
    }
  }

  async addUpdateItem(item: Item) {
    try {
      let sql ='';

      if (item.id) sql = `UPDATE Item SET categoryId=?, code=?, name=?, uom=?, cost=?, price=?, isActive=? WHERE id=?`
      else sql = `INSERT OR IGNORE INTO Item(categoryId, code, name, uom, cost, price) VALUES(?, ?, ?, ?, ?, ?)`;

      sql = sql.replace('?', `${item.categoryId}`);
      sql = sql.replace('?', `'${item.code || ''}'`);
      sql = sql.replace('?', `'${item.name}'`);
      sql = sql.replace('?', `'${item.uom || ''}'`);
      sql = sql.replace('?', `${item.cost}`);
      sql = sql.replace('?', `${item.price}`);
      sql = sql.replace('?', `${item.isActive}`);
      
      if (item.id) sql = sql.replace('?', `${item.id}`);

      await this.db.executeSql(sql);

      setTimeout(() => {
        this.$item.next();
      }, 500);

      return {
        success: true
      };

    } catch (error) {
      
      return {
        success: false,
        message: JSON.stringify(error)
      };

    }
  }
}
