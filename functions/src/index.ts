import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

const cors = require('cors')({ origin: true, }); 

export const validateSubscription = functions.https.onRequest(async (request, response) => {

    const db = admin.firestore();    
    const clientCode = request.query.clientCode;
    const deviceUid = request.query.deviceUid;

    const doc = await db.collection("clients").doc(clientCode).get();
    let data = doc.data();

    if (data) data['success'] = true;

    if (data && deviceUid && !data.deviceUid) {
        
        await db.collection('clients').doc(clientCode).update({
            deviceUid
        });
        
        cors(request, response, () => {
            response.json(data || {});
        });

    } else if (data && deviceUid && data.deviceUid !== deviceUid) {

        cors(request, response, () => {
            response.json({
                success: false,
                message: "Your client code has been used in other device. Contact support to resolve this issue."
            });
        });

    } else {
        cors(request, response, () => {
            response.json(data || {});
        });
    }
})